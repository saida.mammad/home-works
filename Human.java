package com.company;
import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] weekNotes;
    public Human() {
    }
    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    public Human(String name, String surname, int year, int iq, String[][] weekNotes) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.weekNotes = weekNotes;
    }
    public Human getMother() {
        return mother;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getYear() {
        return year;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public int getIq() {
        return iq;
    }
    public void greetPet(Pet pet) {
        System.out.println("Привет, " + pet.getNickname());
    }
    public void describePet(Pet pet) {
        System.out.println("У меня есть " + pet.getSpecies() + ". Eму " + pet.getAge() + " лет/года, он хитрый!");
    }
    public String toString() {
        return "Human{name=" + name + ", surname=" + surname + ", year=" + year + ", iq=" + iq + ", schedule=" + Arrays.deepToString(weekNotes) + "}";
    }
}