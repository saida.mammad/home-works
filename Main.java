package com.company;
import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        Family family = new Family(new Human("Saida", "Salimova"), new Human("Mammad", "Salimov"));
        System.out.println(family);
        family.addChild(new Human("Nazrin", "Salimli"));
        family.addChild(new Human("Kamil", "Salimov"));
        System.out.println(family);
        family.deleteChild(1);
        System.out.println(family);
        System.out.println(family.countFamily());
    }
}