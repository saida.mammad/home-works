package com.company;

import java.util.Arrays;
public class Family {
    private Human mother;
    private Human father;
    private Human[] children = {};
    private Pet pet;
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }
    public Human getMother() {
        return mother;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public Human[] getChildren() {
        return children;
    }
    public void setChildren(Human[] children) {
        this.children = children;
    }
    public Pet getPet() {
        return pet;
    }
    public void setPet6(Pet pet) {
        this.pet = pet;
    }
    public void addChild(Human human) {
        children = addMemmber(children, human);
    }
    public static Human[] addMemmber(Human[] allChild, Human human) {
        allChild = Arrays.copyOf(allChild, allChild.length + 1);
        allChild[allChild.length - 1] = human;
        return allChild;
    }
    public boolean deleteChild(int index) {
        if (index > children.length - 1) return false;
        Human[] childrenResult = new Human[children.length - 1];
        int j = 0;
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                childrenResult[j] = children[i];
                j++;
            }
        }
        children = childrenResult;
        return true;
    }
    public int countFamily() {
        return 2 + children.length;
    }
    @Override
    public String toString() {
        return "Family{" + "mother=" + mother + ", father=" + father + ", children=" + Arrays.toString(children) + ", pet=" + (pet != null ? ("{nickname=" + pet.getNickname() + ", age=" + pet.getAge() + ", trickLevel=" + pet.getTrickLevel() + ", habits=" + Arrays.toString(pet.getHabits())) : null) + "}";
    }
}